import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//ด้านการแสดงความคิดเห็น 2
class ThinkWidget extends StatefulWidget {
  const ThinkWidget({Key? key}) : super(key: key);

  @override
  _ThinkWidgetState createState() => _ThinkWidgetState();
}

class _ThinkWidgetState extends State<ThinkWidget> {
  var thinkValues = [false, false, false, false, false, false, false, false, false, false, false];
  var think = [
    'สังเกตลักษณะส่วนประกอบต่างๆโดยใช้ประสาทสัมผัส',
    'สังเกตสิ่งต่างๆในสถานที่จากมุมมองที่ต่างกัน',
    'สามรถบอกตำแหน่งและทิศทางได้',
    'คัดแยก จัดกลุ่มของที่ต่างชนิดกันได้',
    'นับสิ่งของภายในบ้านใชชีวิตประจำวัน',
    'เปรียบเทียบและเรียงลำดับของจำนวนต่างๆได้',
    'เรียงเหตุการณ์ในชีวิตประจำวันได้',
    'อธิบายถึงสาเหตุและผลที่เกิดขึ้นได้',
    'การคาดเดาอย่างมีเหตุมีผล',
    'มีส่วนรวมในการแสดงความคิดเห็นภายในครอบครัว',
    'ตัดสินใจ มีส่วนร่วมในการช่วยแก้ปัญหาต่างๆ',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ด้านการแสดงความคิดเห็น')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: thinkValues[0],
              title: Text(think[0]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[1],
              title: Text(think[1]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[2],
              title: Text(think[2]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[3],
              title: Text(think[3]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[4],
              title: Text(think[4]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[5],
              title: Text(think[5]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[6],
              title: Text(think[6]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[7],
              title: Text(think[7]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[8],
              title: Text(think[8]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[8] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[9],
              title: Text(think[9]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[9] = newValue!;
                });
              }),
          CheckboxListTile(
              value: thinkValues[10],
              title: Text(think[10]),
              onChanged: (newValue) {
                setState(() {
                  thinkValues[10] = newValue!;
                });
              }),

          ElevatedButton(
              onPressed: () async {
                await _saveThink();
                Navigator.pop(context);
              },
              child: const Text('บันทึก'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadThink();
  }

  Future<void> _loadThink() async {
    var prefs = await SharedPreferences.getInstance();
    var strThinkValue = prefs.getString('think_values') ??
        "[false,false,false,false,false,false,false,false,false,false,false]";
    print(strThinkValue.substring(1, strThinkValue.length - 1));
    var arrStrThinkValues =
        strThinkValue.substring(1, strThinkValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrThinkValues.length; i++) {
        thinkValues[i] = (arrStrThinkValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveThink() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('think_values', thinkValues.toString());
  }
}