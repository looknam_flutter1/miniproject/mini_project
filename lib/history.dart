import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//กรอกประวัติส่วนตัว 3
class HistoryWidget extends StatefulWidget {
  const HistoryWidget({Key? key}) : super(key: key);

  @override
  _HistoryWidgetState createState() => _HistoryWidgetState();
}

class _HistoryWidgetState extends State<HistoryWidget> {
  final _formkey = GlobalKey<FormState>();
  String name = '';
  String gender = 'M'; //M,F
  int age = 0;
  int height = 0;
  int weight = 0;

  final _nameControllor = TextEditingController();
  final _ageControllor = TextEditingController();
  final _heightControllor = TextEditingController();
  final _weightControllor = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadHistory();
  }

  Future<void> _loadHistory() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      _nameControllor.text = name;
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      _ageControllor.text = '$age';
      height = prefs.getInt('height') ?? 1;
      _heightControllor.text = '$height';
      weight = prefs.getInt('weight') ?? 0;
      _weightControllor.text = '$weight';
    });
  }

  Future<void> _saveHistory() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setString('gender', gender);
      prefs.setInt('age', age);
      prefs.setInt('height', height);
      prefs.setInt('weight', weight);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ประวัติส่วนตัว'),
      ),
      body: Form(
          key: _formkey,
          child: Container(
            padding: EdgeInsets.all(16),
            child: ListView(
              children: [
                TextFormField(
                  autofocus: true,
                  controller: _nameControllor,
                  validator: (value) {
                    if (value == null || value.isEmpty || value.length < 5)
                      return 'กรุณาใส่ข้อมูลชื่อ';
                    return null;
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: InputDecoration(labelText: 'ชื่อ-นามสกุล'),
                  onChanged: (value) {
                    setState(() {
                      name = value;
                    });
                  },
                ),
                TextFormField(
                  controller: _ageControllor,
                  validator: (value) {
                    var num = int.tryParse(value!);
                    if (num == null || num <= 0) {
                      return 'กรุณาใส่อายุ ที่มีค่ามากกว่าหรือเท่ากับ 0';
                    }
                    return null;
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: InputDecoration(labelText: 'อายุ'),
                  onChanged: (value) {
                    setState(() {
                      age = int.tryParse(value)!;
                    });
                  },
                  keyboardType: TextInputType.number,
                ),
                DropdownButtonFormField(
                  value: gender,
                  items: [
                    DropdownMenuItem(
                      child: Row(
                        children: [
                          Icon(Icons.male_sharp),
                          SizedBox(width: 8.0),
                          Text('ชาย')
                        ],
                      ),
                      value: 'M',
                    ),
                    DropdownMenuItem(
                      child: Row(
                        children: [
                          Icon(Icons.female_sharp),
                          SizedBox(width: 8.0),
                          Text('หญิง')
                        ],
                      ),
                      value: 'F',
                    )
                  ],
                  onChanged: (String? newValue) {
                    setState(() {
                      gender = newValue!;
                    });
                  },
                ),
                TextFormField(
                  controller: _heightControllor,
                  validator: (value) {
                    var num = int.tryParse(value!);
                    if (num == null || num <= 1) {
                      return 'กรุณาใส่ส่วนสูง ที่มีค่ามากกว่าหรือเท่ากับ 1';
                    }
                    return null;
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: InputDecoration(labelText: 'ส่วนสูง'),
                  onChanged: (value) {
                    setState(() {
                      height = int.tryParse(value)!;
                    });
                  },
                  keyboardType: TextInputType.number,
                ),
                TextFormField(
                  controller: _weightControllor,
                  validator: (value) {
                    var num = int.tryParse(value!);
                    if (num == null || num <= 1) {
                      return 'กรุณาใส่น้ำหนัก ที่มีค่ามากกว่าหรือเท่ากับ 0';
                    }
                    return null;
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: InputDecoration(labelText: 'น้ำหนัก'),
                  onChanged: (value) {
                    setState(() {
                      weight = int.tryParse(value)!;
                    });
                  },
                  keyboardType: TextInputType.number,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formkey.currentState!.validate()) {
                        _saveHistory();
                        Navigator.pop(context);
                      }
                    },
                    child: const Text('บันทึก'))
              ],
            ),
          )),
    );
  }
}
