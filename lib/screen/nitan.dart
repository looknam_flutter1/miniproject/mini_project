import 'package:flutter/material.dart';
//นิทาน
 
class Nitan extends StatefulWidget {
    static const routeName = '/nitan';
 
    @override
    State<StatefulWidget> createState() {
        return _NitanState(title: 'นิทาน');
    }
}
 
class _NitanState extends State<Nitan> {
 _NitanState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('นิทานอีสป',
      style: TextStyle(
      color: Colors.pinkAccent,
      fontWeight: FontWeight.bold,
      fontSize: 100,
      )),  
  );
    @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),

      body: body,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(child: Text('นิทานอีสป',
              style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,)) ,
              decoration: BoxDecoration(color: Colors.pink[100]),
            ),

            ListTile(
              title: const Text('กระต่ายกับเต่า',
              style: TextStyle(
              color: Colors.purpleAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("กระต่ายกับเต่า",
                        style: TextStyle(
                          color: Colors.purpleAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/1.jpg', height: 200, width: 500,
                        ),
                      Text("          กระต่ายตัวหนึ่งมั่นใจในความเร็วของตัวมัน มันท้าทายสัตว์ทุกตัวให้แข่งกับมัน" 
                            "ไม่มีสัตว์ตัวใดรับคำท้า เต่าตัวหนึ่งนึกสนุกจึงรับคำท้าเจ้ากระต่าย"
                            "เมื่อการแข่งเริ่มต้น กระต่ายน้อยวิ่งนำเต่าไปสุดลูกหูลูกตา"
                            "มันไม่เห็นเจ้าเต่าจึงงีบหลับลง เต่าน้อยคลานมาถึงกระต่าย"
                            "กระต่ายก็ไม่รู้ตัว เมื่อกระต่ายตื่นขึ้นมา เจ้าเต่าก็เข้าเส้นชัยเสียแล้ว",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/1.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน ความประมาทนำมาสู่ความปราชัย",
                        style: TextStyle(
                          color: Colors.purpleAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );         
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('ห่านกับไข่ทองคำ',
              style: TextStyle(
              color: Colors.blueAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("ห่านกับไข่ทองคำ",
                        style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/2.jpg', height: 200, width: 500,
                        ),
                      Text("          ชาวนาคนหนึ่งเลี้ยงห่านไว้กินไข่เขาเก็บไข่มากินทุกวัน"
                            "เช้าวันหนึ่งขณะเก็บไข่ห่าน เขาพบว่ามันเป็นสีเหลืองอร่าม"
                            "เมื่อจับดูจึงรู้ว่าว่ามันเป็นไข่ทองคำ ทุกเช้าห่านจะไข่ออกมาวันละฟอง"
                            "เขาจึงทยอยขายไข่นั้นจนร่ำรวย อยู่มาวันหนึ่ง"
                            "เขาเกิดความโลภ จึงจับห่านผ่าท้อง หวังว่าจะเจอไข่มากกว่าเดิม"
                            "เมื่อเขาเปิดออกดูกลับพบแต่ความว่างเปล่าในท้องของห่านตัวนั้น",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/2.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน โลภมากลาภหาย",
                        style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );  
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('หมากับเงา',
              style: TextStyle(
              color: Colors.indigo,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("หมากับเงา",
                        style: TextStyle(
                          color: Colors.indigo,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/3.jpg', height: 200, width: 500,
                        ),
                      Text("          หมาตัวหนึ่งคาบก้อนเนื้อมาก้อนหนึ่งมันเดินผ่านแม่น้ำ"
                            "เห็นเงาตนเองคาบเนื้ออยู่ มันคิดว่าเงาของตนคาบเนื้อชิ้นใหญ่กว่า"
                            "จึงคิดแย่งก้อนเนื้อจากเงานั้น"
                            "เมื่อมันอ้าปากและกำลังกระโจนลงน้ำ ก้อนเนื้อก็หลุดออกตกน้ำไป"
                            "มันจึงเสียก้อนเนื้อไป",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/3.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน โลภมากลาภหาย",
                        style: TextStyle(
                          color: Colors.indigo,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('กบเลือกนาย',
              style: TextStyle(
              color: Colors.greenAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("กบเลือกนาย",
                        style: TextStyle(
                          color: Colors.greenAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/4.jpg', height: 200, width: 500,
                        ),
                      Text("          วันหนึ่งพวกกบมันคิดว่า"
                            "เป็นสิ่งไม่ถูกต้องหากพวกมันไม่มีราชาปกครองตน"
                            "มันจึงทูลขอเทพให้ประทานราชามาปกครองพวกมัน"
                            "เทพได้ยินจึงส่งท่อนซุงลงมา"
                            "เหล่ากบไม่พอใจที่ท่อนซุงอยู่เฉยๆไม่ทำอะไร"
                            "จึงทูลขอเทพอีกครั้งให้ส่งราชาลงมาใหม่"
                            "เทพรำคาญจึงส่งนกกระยางลงไป"
                            "นกกระยางกินกบทีละตัวๆ จนกบหมดจากหนองน้ำ",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/4.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน ไม่มีการปกครองแต่ผาสุข ย่อมดีกว่ามีคนปกครองแต่มอดม้วย",
                        style: TextStyle(
                          color: Colors.greenAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('แมวกับหนู',
              style: TextStyle(
              color: Colors.yellow,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("แมวกับหนู",
                        style: TextStyle(
                          color: Colors.yellow,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/5.jpg', height: 200, width: 500,
                        ),
                      Text("          กาลครั้งหนึ่งนานมาแล้ว มีแมวตัวหนึ่งออกหาจับหนูในบ้านกินทุกวัน"
                           "มันจับหนูกินทีละตัวๆทุุกวัน เจ้าหนูในบ้านนั้นจึงพากันระวังไม่ออกมามาหาอาหารในบ้านอีกเลย" 
                           "และหลบอยู่ในรูของมัน เจ้าแมวเมื่อไม่เห็นหนูหลายวันจนหิวโซจึงออกอุบาย" 
                           "นอนเหยียดแขนขาไม่กระดิกตัวเป็นเวลานาน ราวกับว่ามันสิ้นใจอยู่ไม่ไกลจากรูหนู" 
                           "เจ้าหนูที่คอยเฝ้าดูเจ้าแมวในรูเห็นเจ้าแมวนอนอยู่ดังนั้น ก็ร้องบอกเจ้าแมวว่า"  
                           "เจ้าแมวเจ้าเล่ห์ ถึงเจ้าจะนอนอยู่อย่างนั้นเป็นปีๆพวกข้าก็ไม่ออกไปหาเจ้าหรอก",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/5.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน คนเจ้าเล่ห์ย่อมไม่เป็นที่ไว้ใจของผู้อื่น",
                        style: TextStyle(
                          color: Colors.yellow,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('ชาวนากับงูเห่า',
              style: TextStyle(
              color: Colors.orangeAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("ชาวนากับงูเห่า",
                        style: TextStyle(
                          color: Colors.orangeAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/6.jpg', height: 200, width: 500,
                        ),
                      Text("          ชาวนาคนหนึ่งออกไปทำงานในตอนฤดูหนาว เขาพบงูนอนแข็งใกล้ตายอยู่ระหว่างทางเมื่อถึงบ้าน"
                            "เจ้างูอุ่นขึ้นมันจึงลุกขึ้นและจะกัดลูกของชาวนา ชาวนาเห็นจึงหยิบขวานขึ้นและสับเจ้างูเป็นสองท่อน",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/6.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน ทำคุณกับคนพาลรังแต่เป็นภัย",
                        style: TextStyle(
                          color: Colors.orangeAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('สุนัขจิ้งจอกกับอีกา',
              style: TextStyle(
              color: Colors.redAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("สุนัขจิ้งจอกกับอีกา",
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/7.jpg', height: 200, width: 500,
                        ),
                      Text("          กาลครั้งหนึ่งเจ้าจิ้งจอกเดินทางเข้าไปในป่า" 
                          "มันหิวมากและเหลียวไปเห็นอีกาคาบก้อนเนื้ออยู่บนกิ่งไม้"
                          "มันร้องบอกอีกา “สวัสดีแม่กาสุดสวย” อีกาก้มลงดู"
                          "แม่กาช่างงามนักข้าไม่เคยเห็นนกใดงามเหมือนท่านเลย"
                          "อีกาหลงคำยอและตั้งใจฟังหมาป่า จงอยแม่กาดูงามเช่นกัน ข้าอยากรู้จริงๆว่าเสียงท่านเพราะเช่นไร"
                          "แม่กาหลงไหลคำยอมันผงกหัวขึ้นและอ้าปากเปล่งเสียง"
                          "ทันใดนั้นเนิ้อที่คาบไว้ก็ร่วงลง สุนัขจิ้งจอกรีบคาบเนื้ิอและวิ่งหายไปจากแม่กา",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/7.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน อย่าได้หลงเชื่อคำยกยอ",
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('สุนัขจิ้งจอกกับพวงองุ่น',
              style: TextStyle(
              color: Colors.purpleAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("สุนัขจิ้งจอกกับพวงองุ่น",
                        style: TextStyle(
                          color: Colors.purpleAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/8.jpg', height: 200, width: 500,
                        ),
                      Text("          สุนัขจิ้งจอกตัวหนึ่งกระหายน้ำมาก มันเห็นพวงองุ่น จึงคิดจะกินดับกระหาย"
                            "มันพยายามกระโดด แต่จนแล้วจนเล่า"
                            "มันก็ไม่สามารถกระโดดถึงพวงองุ่น"
                            "มันจึงละความพยายาม และพูดว่า เจ้าองุ่นนี่ต้องเปรี้ยวเกินที่จะกินแน่",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/8.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน คนเราเมื่อไม่ได้สิ่งที่ต้องการมักจะกล่าวโทษผู้อื่น",
                        style: TextStyle(
                          color: Colors.purpleAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('หมีกับนักเดินทาง',
              style: TextStyle(
              color: Colors.blueAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("หมีกับนักเดินทาง",
                        style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/9.jpg', height: 200, width: 500,
                        ),
                      Text("          ชายสองคนเดินทางท่องเที่ยวไปในป่า"
                            "ระหว่างทางหมีตัวใหญ่โผล่มาชายคนแรกด้วยความไวปีนขึ้นต้นไม้"
                            "ชายอีกคนตะโกนร้องเรียกให้ช่วยดึงเขาขึ้นไป แต่เพื่อนเขาปฏิเสธ"
                            "เมื่อหมีเข้ามาใกล้ ชายคนที่สองจึงแกล้งทำเป็นตายหมีเดินดมดู"
                            "เมื่อดมที่หูเสร็จหมีก็เดินจากไป เพราะคิดว่าเขาตายแล้ว"
                            "หลังจากหมีจากไป",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/9.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน เพื่อนแท้ย่อมไม่เอาตัวรอดเพียงลำพัง",
                        style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('ไก่ได้พลอย',
              style: TextStyle(
              color: Colors.indigo,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("ไก่ได้พลอย",
                        style: TextStyle(
                          color: Colors.indigo,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/10.jpg', height: 200, width: 500,
                        ),
                      Text("          เช้าวันหนึ่งขณะที่พ่อไก่กำลังคุ้ยเขี่ยหาอาหารบนลานดิน"
                            "พ่อไก่ก็เขี่ยเจอพลอยเม็ดงามส่องประกายวิบวับจึงรำพึงขึ้นว่านี่ถ้าช่างทองได้พลอยงามเม็ดนี้"  
                            "เขาคงจะดีใจมากแต่สำหรับข้าแล้วข้าวแค่เม็ดเดียวยังมีค่ามากกว่าพลอยเม็ดโตเสียอีก"
                            "ในที่สุดพ่อไก่ก็เลิกสนใจพลอยเม็ดนั้นหันไปก้มหน้าก้มตาคุ้ยเขี่ยหาอาหารของตนต่อไป",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/10.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน ของมีค่าถ้าตกอยู่กับผู้ไม่รู้คุณค่าย่อมไร้ประโยชน์",
                        style: TextStyle(
                          color: Colors.indigo,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('อินทรีกับลูกศร',
              style: TextStyle(
              color: Colors.greenAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("อินทรีกับลูกศร",
                        style: TextStyle(
                          color: Colors.greenAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/11.jpg', height: 200, width: 500,
                        ),
                      Text("          มีอินทรีตัวใหญ่ตัวหนึ่งเกาะอยู่ที่ก้อนหินเหนือหน้าผาแห่งหนึ่งในหุบเขา" 
                            "พลันมีลูกศรพุ่งตรงมาที่มันโยไม่ทันคาดคิด เจ้าลูกศรพุ่งตรงปักเข้าไปยังอกของมันทะลุไปยังหัวใจ"
                            "ของนกอิทรีอย่างแม่นยำรามจับวางเจ้านกอินทรีใหญ่ล้มลงบนก้อนหินเจ้านกอินทรีย์ร้องออกมาก่อนสิ้นใจ" 
                            "ข้าต้องมาตายเพราะขนจากพวกของข้าเอง ช่างน่าเศร้าจริงๆ",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/11.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน สิ่งใดที่เราทำอาจย้อนมาทำร้ายเราได้ทุกเมื่อ",
                        style: TextStyle(
                          color: Colors.greenAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('ลูกปูและแม่ปู',
              style: TextStyle(
              color: Colors.yellow,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("ลูกปูและแม่ปู",
                        style: TextStyle(
                          color: Colors.yellow,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/12.jpg', height: 200, width: 500,
                        ),
                      Text("           อะไรทำให้ลูกเดินเป๋แบบนั้นแม่ปูบอกกับลูกชายของมัน ลูกน่าจะเดินให้มันตรงๆ" 
                            "โดยหันนิ้วออกไปสิ แม่จ๋า ลองเดินให้หนูดูหน่อย ลูกปูตอบอย่างเชื่อฟังหนูอยากจะเรียนรู้"
                            "แม่ปูจึงพยายามเดินให้ตรงซ้ำแล้วซ้ำเล่าแต่มันก็ทำได้เพียงเดินเฉไปข้างหนึ่งเหมือนลูกชายของมันเท่านั้น" 
                            "และเมื่อมันพยายามจะหันนิ้วออกไปข้างหน้ามันก็สะดุดและล้มคะมำลงบนพื้น",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/12.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน ตัวอย่างที่ดีมีค่ากว่าคำสอน การจะทำอะไรที่ฝืนสัญชาติตัวเองย่อมเป็นไปไม่ได้ฉันนั้น",
                        style: TextStyle(
                          color: Colors.yellow,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('มดกับนกพิราบ',
              style: TextStyle(
              color: Colors.orangeAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("มดกับนกพิราบ",
                        style: TextStyle(
                          color: Colors.orangeAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/13.jpg', height: 200, width: 500,
                        ),
                      Text("          มีเจ้ามดลื่นไหลลงสู่ลำธารนั้นมันพยายามตะเกียกตะกายขึ้นจากลำะารนั้น" 
                            "แต่มันก็ไม่สามารถนำตนเองฝืนกระแสน้ำในลำธารนั้นได้" 
                            "แล้วนกพิราบตัวหนึ่งบินผ่านมา มันจึงเด็ดใบไม้ลงไปให้มดเกาะ"
                            "เจ้ามดเกาะใบไม้และขึ้นฝั่งได้อย่างปลอดภัย มีนายพรานคนหนึ่งเห็นเจ้าพิราบเข้า" 
                            "จึงง้างคันศรหวังจะยิงนกพิราบไปเป็นอาหาร เจ้ามดน้อยเห็นเข้าจึงรีบวิ่งไปกัดบนเท้านายพราน"
                            "ลูกศรพลาดเจ้านกพิราบ เจ้านกพิราบรู้ตัวจึงบินหนีไป",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/13.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน คนที่ไม่สำคัญอาจจะช่วยท่านได้ในยามที่จำเป็น",
                        style: TextStyle(
                          color: Colors.orangeAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

            ListTile(
              title: const Text('เด็กโลภ',
              style: TextStyle(
              color: Colors.redAccent,
              fontWeight: FontWeight.bold,
              fontSize: 15,)),
              onTap: (){
                setState(() {
                  var center = Center(
                    child: Column(children: <Widget>[
                      Text("เด็กโลภ",
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),),
                      Image.asset('images/14.jpg', height: 200, width: 500,
                        ),
                      Text("          กาลครั้งหนึ่งมีเด็กชายคนหนึ่งเห็นโถแก้วบรรจุถั่วเปลือกแข็งไว้เต็มโถ" 
                            "เขาจึงล้วงมือไปในโถแก้วนั้นแต่ช่างน่าเศร้า มือของเขาติดอยู่กับปากโถแก้ว" 
                            "เด็กน้อยจึงไม่สามารถเอามือของตนออกจากปากโถแก้วนั้นได้เลย" 
                            "เด็กน้อยได้แต่ล้มลงอยู่ตรงพื้นที่วางโถแก้วไว้และร้องไห้อยู่ตรงนั้น",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )
                          ,),
                      Image.asset('images/14.gif', height: 100, width: 100,
                        ),
                      Text("คติจากนิทาน คนเราไม่ควรโลภเอาอะไรในทีเดียว",
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),),
                    ]),
                  );
                  body = center;
                });
                Navigator.pop(context);
              },
            ),

          ],
          ),
      ),
    );
  }
}