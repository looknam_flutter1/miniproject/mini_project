import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../study.dart';
import '../body.dart';
import '../language.dart';
import '../think.dart';
//แสดงทักษะเด็ก
class Skill extends StatefulWidget {
  static const routeName = '/skill';

  @override
  State<StatefulWidget> createState() {
    return _SkillState();
  }
}

class _SkillState extends State<Skill> {
  var studyScore = 0.0;
  var bodyScore = 0.0;
  var languageScore = 0.0;
  var thinkScore = 0.0;
  @override
  void initState() {
    super.initState();
    _loadStudy();
    _loadBody();
    _loadLanguage();
    _loadThink();
  }

  Future<void> _loadStudy() async {
    var prefs = await SharedPreferences.getInstance();
    var strStudyValue = prefs.getString('study_values') ??
        "[false,false,false,false,false,false,false,false,false,false,false]";
    print(strStudyValue.substring(1, strStudyValue.length - 1));
    var arrStrStudyValues =
        strStudyValue.substring(1, strStudyValue.length - 1).split(',');
    setState(() {
      studyScore = 0.0;
      for (var i = 0; i < arrStrStudyValues.length; i++) {
        if (arrStrStudyValues[i].trim() == 'true') {
          studyScore = studyScore + 1;
        }
      }
      studyScore = (studyScore * 100) / 11;
    });
  }

  Future<void> _loadBody() async {
    var prefs = await SharedPreferences.getInstance();
    var strBodyValue = prefs.getString('body_values') ??
        "[false,false,false,false,false,false,false,false,false]";
    print(strBodyValue.substring(1, strBodyValue.length - 1));
    var arrStrBodyValues =
        strBodyValue.substring(1, strBodyValue.length - 1).split(',');
    setState(() {
      bodyScore = 0.0;
      for (var i = 0; i < arrStrBodyValues.length; i++) {
        if (arrStrBodyValues[i].trim() == 'true') {
          bodyScore = bodyScore + 1;
        }
      }
      bodyScore = (bodyScore * 100) / 9;
    });
  }

  Future<void> _loadLanguage() async {
    var prefs = await SharedPreferences.getInstance();
    var strLanguageValue = prefs.getString('language_values') ??
        "[false]";
    print(strLanguageValue.substring(1, strLanguageValue.length - 1));
    var arrStrLanguageValues =
        strLanguageValue.substring(1, strLanguageValue.length - 1).split(',');
    setState(() {
      languageScore = 0.0;
      for (var i = 0; i < arrStrLanguageValues.length; i++) {
        if (arrStrLanguageValues[i].trim() == 'true') {
          languageScore = languageScore + 1;
        }
      }
      languageScore = (languageScore * 100) / 8;
    });
  }

  Future<void> _loadThink() async {
    var prefs = await SharedPreferences.getInstance();
    var strThinkValue = prefs.getString('think_values') ??
        "[false]";
    print(strThinkValue.substring(1, strThinkValue.length - 1));
    var arrStrThinkValues =
        strThinkValue.substring(1, strThinkValue.length - 1).split(',');
    setState(() {
      thinkScore = 0.0;
      for (var i = 0; i < arrStrThinkValues.length; i++) {
        if (arrStrThinkValues[i].trim() == 'true') {
          thinkScore = thinkScore + 1;
        }
      }
      thinkScore = (thinkScore * 100) / 11;
    });
  }

  Future<void> _resetStudy() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('study_values');
    prefs.remove('body_values');
    prefs.remove('language_values');
    prefs.remove('think_values');
    await _loadStudy();
    await _loadBody();
    await _loadLanguage();
    await _loadThink();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ทักษะพื้นฐาน'),
      ),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'การเรียนรู้ : ${studyScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontWeight: (studyScore > 50)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 24.0,
                        color: (studyScore > 50)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.green.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'สุขภาพร่างกาย : ${bodyScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontWeight: (bodyScore > 50)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 24.0,
                        color: (bodyScore > 50)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.green.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'ฟังภาษา : ${languageScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontWeight: (languageScore > 50)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 24.0,
                        color: (languageScore > 50)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.green.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'ความคิด : ${thinkScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontWeight: (thinkScore > 50)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 24.0,
                        color: (thinkScore > 50)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.green.withOpacity(0.6)),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () async {
                        await _resetStudy();
                      },
                      child: const Text('ลบข้อมูล'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Text('ด้านการเรียนรู้'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => StudyWidget()));
              await _loadStudy();
            },
          ),
          ListTile(
            title: Text('ด้านสุขภาพร่างกาย'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => BodyWidget()));
              await _loadBody();
            },
          ),
          ListTile(
            title: Text('ด้านการฟังภาษา'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LanguageWidget()));
              await _loadLanguage();
            },
          ),
          ListTile(
            title: Text('ด้านการแสดงความคิดเห็น'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ThinkWidget()));
              await _loadThink();
            },
          ),
        ], 
      ),
    );
  }
}
