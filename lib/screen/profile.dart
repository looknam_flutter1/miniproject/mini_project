import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../color.dart';
import '../history.dart';
import '../movie.dart';
import '../object.dart';
//แสดงข้อมูลเด็ก

class Profile extends StatefulWidget {
  static const routeName = '/profile';

  const Profile({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ProfileState();
  }
}

class _ProfileState extends State<Profile> {
  String name = '';
  String gender = 'M';
  int age = 0;
  int height = 1;
  int weight = 0;
  var likes = ['-', '-', '-'];
  var objects = ['-', '-', '-'];
   var movies = ['-', '-', '-'];

  @override
  void initState() {
    super.initState();
    _loadHistory();
  }

  Future<void> _loadHistory() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      height = prefs.getInt('height') ?? 1;
      weight = prefs.getInt('weight') ?? 0;
      likes = prefs.getStringList('colors') ?? ['-', '-', '-'];
      objects = prefs.getStringList('objects') ?? ['-', '-', '-'];
      movies = prefs.getStringList('movies') ?? ['-', '-', '-'];
    });
  }

  Future<void> _resetHistory() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('gender');
    prefs.remove('age');
    prefs.remove('height');
    prefs.remove('weight');
    prefs.remove('colors');
    prefs.remove('objects');
    prefs.remove('movies');
    await _loadHistory();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    var string = name;
    return Scaffold(
      appBar: AppBar(
        title: Text('ข้อมูลส่วนตัว'),
      ),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.sentiment_very_satisfied_rounded),
                  title: Text(name),
                  subtitle: Text('เพศ: $gender, อายุ: $age ปี, ส่วนสูง: $height เซนติเมตร, น้ำหนัก: $weight กิโลกรัม',
                      style: TextStyle(
                      color: Colors.pinkAccent.withOpacity(0.6),
                      fontWeight: FontWeight.bold,
                      fontSize: 15,)),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'สีที่ชอบ : ${likes.toString()}',
                      style: TextStyle(
                      color: Colors.pinkAccent.withOpacity(0.6),
                      fontWeight: FontWeight.bold,
                      fontSize: 20,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'ของที่อยากได้ : ${objects.toString()}',
                      style: TextStyle(
                      color: Colors.pinkAccent.withOpacity(0.6),
                      fontWeight: FontWeight.bold,
                      fontSize: 20,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'หนังที่ชอบ : ${movies.toString()}',
                      style: TextStyle(
                      color: Colors.pinkAccent.withOpacity(0.6),
                      fontWeight: FontWeight.bold,
                      fontSize: 20,),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () async {
                        await _resetHistory();
                      },
                      child: const Text('ลบ'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Text('ประวัติส่วนตัว'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HistoryWidget()));
              await _loadHistory();
            },
          ),
          ListTile(
              title: Text('สีที่ชอบ'),
              onTap: () async {
                await Navigator.push(context,
                   MaterialPageRoute(builder: (context) => ColorWidget()));
                await _loadHistory();
            },
          ),
          ListTile(
              title: Text('ของที่อยากได้'),
              onTap: () async {
                await Navigator.push(context,
                   MaterialPageRoute(builder: (context) => ObjectWidget()));
                await _loadHistory();
            },
          ),
          ListTile(
              title: Text('หนังที่ชอบ'),
              onTap: () async {
                await Navigator.push(context,
                   MaterialPageRoute(builder: (context) => MovieWidget()));
                await _loadHistory();
            },
          ),
        ],
      ),
    );
  }
}
