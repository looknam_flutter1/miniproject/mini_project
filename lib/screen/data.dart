import 'package:flutter/material.dart';
import 'nitan.dart';
import 'skill.dart';
import 'profile.dart';
//ปุ่มด้านล่าง
class Data extends StatefulWidget {
  static const routeName = '/';

  @override
  State<StatefulWidget> createState() {
    return _DataState();
  }
}

class _DataState extends State<Data> {
  int _currentIndex = 0;
  List<Widget> _pageWidget = <Widget>[
    Nitan(),
    Skill(),
    Profile(),
  ];

  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageWidget.elementAt(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        backgroundColor: Colors.pinkAccent,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white.withOpacity(.6),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (value) {
          // Respond to item press.
          setState(() => _currentIndex = value);
        },
        items: [
          BottomNavigationBarItem(
            title: Text('นิทาน'),
            icon: Icon(Icons.menu_book_rounded),
          ),
          BottomNavigationBarItem(
            title: Text('ทักษะ'),
            icon: Icon(Icons.extension_rounded),
          ),
          BottomNavigationBarItem(
            title: Text('ข้อมูลเด็ก'),
            icon: Icon(Icons.assignment_ind_rounded),
          ),
        ],
      ),
    );
  }
}
