import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//ด้านการเรียนรู้ 2
class StudyWidget extends StatefulWidget {
  const StudyWidget({Key? key}) : super(key: key);

  @override
  _StudyWidgetState createState() => _StudyWidgetState();
}

class _StudyWidgetState extends State<StudyWidget> {
  var studyValues = [false, false, false, false, false, false, false, false, false, false, false];
  var study = [
    'จดจำ ก-ฮ ได้',
    'จดจำ A-Z ได้',
    'นับเลข 1-100 ได้',
    'บวกเลขคณิตศาสตร์ได้',
    'สามารถแยกสีต่างๆได้',
    'ดูรูปภาพแล้วตอบได้ว่าสิ่งนั้นคืออะไร',
    'ต่อจิ๊กซอว์เป็นรูปภาพได้',
    'สามารถบอกชื่อเล่น ชื่อจริง นามสกุลของตัวเองได้',
    'บอกชื่ออวัยวะต่างๆในร่างกายได้',
    'รู้จักกล่าวคำว่า สวัสดี ขอบคุณ',
    'คัดลายมือตัวบรรจงเต็มบรรทัดได้'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ด้านการเรียนรู้')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: studyValues[0],
              title: Text(study[0]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[1],
              title: Text(study[1]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[2],
              title: Text(study[2]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[3],
              title: Text(study[3]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[4],
              title: Text(study[4]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[5],
              title: Text(study[5]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[6],
              title: Text(study[6]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[7],
              title: Text(study[7]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[8],
              title: Text(study[8]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[8] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[9],
              title: Text(study[9]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[9] = newValue!;
                });
              }),
          CheckboxListTile(
              value: studyValues[10],
              title: Text(study[10]),
              onChanged: (newValue) {
                setState(() {
                  studyValues[10] = newValue!;
                });
              }),

          ElevatedButton(
              onPressed: () async {
                await _saveStudy();
                Navigator.pop(context);
              },
              child: const Text('บันทึก'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadStudy();
  }

  Future<void> _loadStudy() async {
    var prefs = await SharedPreferences.getInstance();
    var strStudyValue = prefs.getString('study_values') ??
        "[false,false,false,false,false,false,false,false,false,false,false]";
    print(strStudyValue.substring(1, strStudyValue.length - 1));
    var arrStrStudyValues =
        strStudyValue.substring(1, strStudyValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrStudyValues.length; i++) {
        studyValues[i] = (arrStrStudyValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveStudy() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('study_values', studyValues.toString());
  }
}
