import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//ด้านสุขภาพร่างกาย 2
class BodyWidget extends StatefulWidget {
  const BodyWidget({Key? key}) : super(key: key);

  @override
  _BodyWidgetState createState() => _BodyWidgetState();
}

class _BodyWidgetState extends State<BodyWidget> {
  var bodyValues = [false, false, false, false, false, false, false, false, false];
  var body = [
    'น้ำหนักส่วนสูงตามเกณฑ์กรมอนามัย',
    'รับประทานอาหารที่มีประโยชน์และดื่มน้ำสะอาด',
    'ล้างมือก่อนรับประทานอาหาร',
    'นอนพักผ่อนเป็นเวลา',
    'ออกกำลังกายเป็นเวลา',
    'สามารถเดินถอยหลังได้',
    'กระโดดขาเดียวได้',
    'วิ่งแล้วหยุด วิ่งแล้วหลบสิ่งกีดขวางได้',
    'รับลูกบอลด้วยมือได้',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ด้านสุขภาพร่างกาย')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: bodyValues[0],
              title: Text(body[0]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: bodyValues[1],
              title: Text(body[1]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: bodyValues[2],
              title: Text(body[2]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: bodyValues[3],
              title: Text(body[3]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: bodyValues[4],
              title: Text(body[4]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: bodyValues[5],
              title: Text(body[5]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: bodyValues[6],
              title: Text(body[6]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: bodyValues[7],
              title: Text(body[7]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: bodyValues[8],
              title: Text(body[8]),
              onChanged: (newValue) {
                setState(() {
                  bodyValues[8] = newValue!;
                });
              }),

          ElevatedButton(
              onPressed: () async {
                await _saveBody();
                Navigator.pop(context);
              },
              child: const Text('บันทึก'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadBody();
  }

  Future<void> _loadBody() async {
    var prefs = await SharedPreferences.getInstance();
    var strBodyValue = prefs.getString('body_values') ??
        "[false,false,false,false,false,false,false,false,false]";
    print(strBodyValue.substring(1, strBodyValue.length - 1));
    var arrStrBodyValues =
        strBodyValue.substring(1, strBodyValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrBodyValues.length; i++) {
        bodyValues[i] = (arrStrBodyValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveBody() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('body_values', bodyValues.toString());
  }
}
