import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//สีที่ชอบ 3
class ColorWidget extends StatefulWidget {
  const ColorWidget({Key? key}) : super(key: key);

  @override
  _ColorWidgetState createState() => _ColorWidgetState();
}

class _ColorWidgetState extends State<ColorWidget> {
  var colors = ['-', '-', '-'];
  @override
  void initState() {
    super.initState();
    _loadColors();
  }

  _loadColors() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      colors = prefs.getStringList('colors') ?? ['-', '-', '-'];
    });
  }

  _saveColors() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('colors', colors);
    });
  }

  Widget _colorComboBox(
      {required String title,
      required String value,
      ValueChanged<String?>? onChanged}) {
    return Row(
      children: [
        Text(title),
        Expanded(
          child: Container(),
        ),
        DropdownButton(
          items: [
            DropdownMenuItem(
              child: Text('-'),
              value: '-',
            ),
            DropdownMenuItem(child: Text('ชมพู'), value: 'ชมพู'),
            DropdownMenuItem(child: Text('ฟ้า'), value: 'ฟ้า'),
            DropdownMenuItem(child: Text('เขียว'), value: 'เขียว'),
            DropdownMenuItem(child: Text('เหลือง'), value: 'เหลือง'),
            DropdownMenuItem(child: Text('ส้ม'), value: 'ส้ม'),
            DropdownMenuItem(child: Text('แดง'), value: 'แดง'),
            DropdownMenuItem(child: Text('ม่วง'), value: 'ม่วง'),
            DropdownMenuItem(child: Text('น้ำเงิน'), value: 'น้ำเงิน'),
            DropdownMenuItem(child: Text('ขาว'), value: 'ขาว'),
            DropdownMenuItem(child: Text('ดำ'), value: 'ดำ'),
            DropdownMenuItem(child: Text('น้ำตาล'), value: 'น่ำตาล'),
          ],
          value: value,
          onChanged: onChanged,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('สีที่ชอบ'),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            _colorComboBox(
                title: 'สีที่ 1',
                value: colors[0],
                onChanged: (newValue) {
                  setState(() {
                    colors[0] = newValue!;
                  });
                }),
            _colorComboBox(
                title: 'สีที่ 2',
                value: colors[1],
                onChanged: (newValue) {
                  setState(() {
                    colors[1] = newValue!;
                  });
                }),
            _colorComboBox(
                title: 'สีที่ 3',
                value: colors[2],
                onChanged: (newValue) {
                  setState(() {
                    colors[2] = newValue!;
                  });
                }),
            ElevatedButton(
                onPressed: () {
                  _saveColors();
                  Navigator.pop(context);
                },
                child: Text('บันทึก'))
          ],
        ),
      ),
    );
  }
}
