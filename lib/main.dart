import 'package:flutter/material.dart';
import 'screen/data.dart';
 
void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
                theme: ThemeData(
                    primaryColor: Colors.pinkAccent,
                    accentColor: Colors.pinkAccent,
                ),
                title: 'Flutter App Mini Project',
                initialRoute: '/', 
                routes: {
                    Data.routeName: (context) => Data(),
                },
        );
    }
}