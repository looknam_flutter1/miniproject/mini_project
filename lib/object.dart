import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//ของที่อยากได้ 3
class ObjectWidget extends StatefulWidget {
  const ObjectWidget({Key? key}) : super(key: key);

  @override
  _ObjectWidgetState createState() => _ObjectWidgetState();
}

class _ObjectWidgetState extends State<ObjectWidget> {
  var objects = ['-', '-', '-'];
  @override
  void initState() {
    super.initState();
    _loadObjects();
  }

  _loadObjects() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      objects = prefs.getStringList('objects') ?? ['-', '-', '-'];
    });
  }

  _saveObjects() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('objects', objects);
    });
  }

  Widget _objectComboBox(
      {required String title,
      required String value,
      ValueChanged<String?>? onChanged}) {
    return Row(
      children: [
        Text(title),
        Expanded(
          child: Container(),
        ),
        DropdownButton(
          items: [
            DropdownMenuItem(
              child: Text('-'),
              value: '-',
            ),
            DropdownMenuItem(child: Text('ตุ๊กตา หรือของเล่น'), value: 'ตุ๊กตา หรือของเล่น'),
            DropdownMenuItem(child: Text('อุปกรณ์เครื่องเขียน'), value: 'อุปกรณ์เครื่องเขียน'),
            DropdownMenuItem(child: Text('อุปกรณ์กีฬา'), value: 'อุปกรณ์กีฬา'),
            DropdownMenuItem(child: Text('อุปกรณ์ดนตรี'), value: 'อุปกรณ์ดนตรี'),
            DropdownMenuItem(child: Text('มือถือ i-pad หรือโน้ตบุ๊ค'), value: 'มือถือ i-pad หรือโน้ตบุ๊ค'),
            DropdownMenuItem(child: Text('เสื้อผ้า'), value: 'เสื้อผ้า'),
            DropdownMenuItem(child: Text('สัตว์เลี้ยง'), value: 'สัตว์เลี้ยง'),
            DropdownMenuItem(child: Text('อุปกรณ์ทำอาหาร สำหรับเด็ก'), value: 'อุปกรณ์ทำอาหาร สำหรับเด็ก'),
            DropdownMenuItem(child: Text('คอร์สเรียน'), value: 'คอร์สเรียน'),
            DropdownMenuItem(child: Text('เงิน'), value: 'เงิน'),
          ],
          value: value,
          onChanged: onChanged,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ของที่อยากได้'),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            _objectComboBox(
                title: 'ของชิ้นที่ 1',
                value: objects[0],
                onChanged: (newValue) {
                  setState(() {
                    objects[0] = newValue!;
                  });
                }),
            _objectComboBox(
                title: 'ของชิ้นที่ 2',
                value: objects[1],
                onChanged: (newValue) {
                  setState(() {
                    objects[1] = newValue!;
                  });
                }),
            _objectComboBox(
                title: 'ของชิ้นที่ 3',
                value: objects[2],
                onChanged: (newValue) {
                  setState(() {
                    objects[2] = newValue!;
                  });
                }),
            ElevatedButton(
                onPressed: () {
                  _saveObjects();
                  Navigator.pop(context);
                },
                child: Text('บันทึก'))
          ],
        ),
      ),
    );
  }
}
