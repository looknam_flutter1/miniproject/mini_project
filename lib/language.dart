import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//ด้สนการฟัง 3
class LanguageWidget extends StatefulWidget {
  const LanguageWidget({Key? key}) : super(key: key);

  @override
  _LanguageWidgetState createState() => _LanguageWidgetState();
}

class _LanguageWidgetState extends State<LanguageWidget> {
  var languageValues = [false, false, false, false, false, false, false, false];
  var language = [
    'ฟังและโต้ตอบ เชื่อมเรื่องที่ฟังได้',
    'เล่าเรื่องราวต่อเนื่องได้',
    'ฟังเสียงต่างๆในสิ่งแวดล้อม',
    'ฟังและปฎิบัติตามคำแนะนำ',
    'ฟังเพลง ฟังนิทาน และเรื่องราวต่างๆได้',
    'พูดแสดงความคิด ความต้องการ ความรู้สึก',
    'พูดเกี่ยวกับประสบการณ์ของตนเองได้',
    'อธิบายเหตุการณ์ต่างๆได้',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ด้านการฟังภาษา')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: languageValues[0],
              title: Text(language[0]),
              onChanged: (newValue) {
                setState(() {
                  languageValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: languageValues[1],
              title: Text(language[1]),
              onChanged: (newValue) {
                setState(() {
                  languageValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: languageValues[2],
              title: Text(language[2]),
              onChanged: (newValue) {
                setState(() {
                  languageValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: languageValues[3],
              title: Text(language[3]),
              onChanged: (newValue) {
                setState(() {
                  languageValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: languageValues[4],
              title: Text(language[4]),
              onChanged: (newValue) {
                setState(() {
                  languageValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: languageValues[5],
              title: Text(language[5]),
              onChanged: (newValue) {
                setState(() {
                  languageValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: languageValues[6],
              title: Text(language[6]),
              onChanged: (newValue) {
                setState(() {
                  languageValues[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: languageValues[7],
              title: Text(language[7]),
              onChanged: (newValue) {
                setState(() {
                  languageValues[7] = newValue!;
                });
              }),
          

          ElevatedButton(
              onPressed: () async {
                await _saveLanguage();
                Navigator.pop(context);
              },
              child: const Text('บันทึก'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadLanguage();
  }

  Future<void> _loadLanguage() async {
    var prefs = await SharedPreferences.getInstance();
    var strLanguageValue = prefs.getString('language_values') ??
        "[false,false,false,false,false,false,false,false]";
    print(strLanguageValue.substring(1, strLanguageValue.length - 1));
    var arrStrLanguageValues =
        strLanguageValue.substring(1, strLanguageValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrLanguageValues.length; i++) {
        languageValues[i] = (arrStrLanguageValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveLanguage() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('language_values', languageValues.toString());
  }
}
