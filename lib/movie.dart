import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//หนังที่ชอบ 3
class MovieWidget extends StatefulWidget {
  const MovieWidget({Key? key}) : super(key: key);

  @override
  _MovieWidgetState createState() => _MovieWidgetState();
}

class _MovieWidgetState extends State<MovieWidget> {
  var movies = ['-', '-', '-'];
  @override
  void initState() {
    super.initState();
    _loadMovies();
  }

  _loadMovies() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      movies = prefs.getStringList('ovies') ?? ['-', '-', '-'];
    });
  }

  _saveMovies() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('movies', movies);
    });
  }

  Widget _movieComboBox(
      {required String title,
      required String value,
      ValueChanged<String?>? onChanged}) {
    return Row(
      children: [
        Text(title),
        Expanded(
          child: Container(),
        ),
        DropdownButton(
          items: [
            DropdownMenuItem(
              child: Text('-'),
              value: '-',
            ),
            DropdownMenuItem(child: Text('แก้วหน้าม้า'), value: 'แก้วหน้าม้า'),
            DropdownMenuItem(child: Text('ปลาบู่ทอง'), value: 'ปลาบู่ทอง'),
            DropdownMenuItem(child: Text('เทพสามฤดู'), value: 'เทพสามฤดู'),
            DropdownMenuItem(child: Text('เงาะป่า'), value: 'เงาะป่า'),
            DropdownMenuItem(child: Text('พญาคันคาก'), value: 'พญาคันคาก'),
            DropdownMenuItem(child: Text('นางสิบสอง'), value: 'นางสิบสอง'),
            DropdownMenuItem(child: Text('พิกุลทอง'), value: 'พิกุลทอง'),
            DropdownMenuItem(child: Text('สิงหไกรภพ'), value: 'สิงหไกรภพ'),
            DropdownMenuItem(child: Text('สังข์ทอง'), value: 'สังข์ทอง'),
            DropdownMenuItem(child: Text('ขวานฟ้าหน้าดำ'), value: 'ขวานฟ้าหน้าดำ'),
            DropdownMenuItem(child: Text('สี่ยอดกุมาร'), value: 'สี่ยอดกุมาร'),
            DropdownMenuItem(child: Text('พระรถเมรี'), value: 'พระรถเมรี'),
          ],
          value: value,
          onChanged: onChanged,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('หนังที่ชอบดู'),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            _movieComboBox(
                title: 'หนังเรื่องที่ 1',
                value: movies[0],
                onChanged: (newValue) {
                  setState(() {
                    movies[0] = newValue!;
                  });
                }),
            _movieComboBox(
                title: 'หนังเรื่องที่ 2',
                value: movies[1],
                onChanged: (newValue) {
                  setState(() {
                    movies[1] = newValue!;
                  });
                }),
            _movieComboBox(
                title: 'หนังเรื่องที่ 3',
                value: movies[2],
                onChanged: (newValue) {
                  setState(() {
                    movies[2] = newValue!;
                  });
                }),
            ElevatedButton(
                onPressed: () {
                  _saveMovies();
                  Navigator.pop(context);
                },
                child: Text('บันทึก'))
          ],
        ),
      ),
    );
  }
}
